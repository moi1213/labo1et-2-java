/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classesinterface;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author moi
 */
public class bdModif implements IModif{

    @Override
    public void modifLegume(String nom, double prix) {
              try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String rech = "update legume set prix = ?  where nom = ?;";
            PreparedStatement cmd = conn.prepareStatement(rech);
            cmd.setString(2,nom);
            cmd.setDouble(1,prix);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifDetergent(String nom, double prix) {
                   try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String rech = "update lessive set prix = ?  where nom = ?;";
            PreparedStatement cmd = conn.prepareStatement(rech);
            cmd.setString(2,nom);
            cmd.setDouble(1,prix);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifViande(String nom, double prix) {
                      try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String rech = "update viande set prix = ?  where nom = ?;";
            PreparedStatement cmd = conn.prepareStatement(rech);
            cmd.setString(2,nom);
            cmd.setDouble(1,prix);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}
