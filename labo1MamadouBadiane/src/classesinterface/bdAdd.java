/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classesinterface;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author moi
 */
public class bdAdd implements IAjout{

    @Override
    public void addLegume(String nom,double prix,String type) {
        try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String insertion = "insert into legume(nom,prix,type)"+
            "values(?,?,?);";
            PreparedStatement cmd = conn.prepareStatement(insertion);
            cmd.setString(1,nom);          
            cmd.setDouble(2,prix);
            cmd.setString(3,type);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void addDetergent(String nom,double prix,String marque) {
        try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String insertion = "insert into lessive(nom,prix,marque)"+
            "values(?,?,?);";
            PreparedStatement cmd = conn.prepareStatement(insertion);
            cmd.setString(1,nom);          
            cmd.setDouble(2,prix);
            cmd.setString(3,marque);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void addViande(String nom,double prix,String animal) {
            try {
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            String insertion = "insert into viande(nom,prix,animal)"+
            "values(?,?,?);";
            PreparedStatement cmd = conn.prepareStatement(insertion);
            cmd.setString(1,nom);          
            cmd.setDouble(2,prix);
            cmd.setString(3,animal);
            cmd.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(bdAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
