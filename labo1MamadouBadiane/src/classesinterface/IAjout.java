/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classesinterface;

/**
 *
 * @author moi
 */
public interface IAjout {
    public void addLegume (String nom, double prix , String type);
    public void addDetergent (String nom, double prix , String marque);
    public void addViande (String nom, double prix , String animal);
}
