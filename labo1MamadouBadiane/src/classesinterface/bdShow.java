/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classesinterface;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author moi
 */
public class bdShow  implements IShow  {

    @Override
    public void showLegume() {
        try {
            String titre[] ={"ID","NOM","PRIX","TYPE"};
            String aff = "select * from legume;";
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            Statement cmd1 = conn.createStatement();
            ResultSet result1 = cmd1.executeQuery(aff);
            
            epic.table.setRowCount(0);
            epic.table.setColumnIdentifiers(titre);
            Object[] row = new Object[4];
            while(result1.next())
            {
                row[0]= result1.getInt("id");
                row[1]= result1.getString("nom");
                row[2]= result1.getString("prix");
                row[3]= result1.getString("type");                
                epic.table.setColumnCount(4);
                epic.table.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(bdShow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void showDetergent() {
               try {
            String titre[] ={"ID","NOM","PRIX","MARQUE"};
            String aff = "select * from lessive;";
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            Statement cmd1 = conn.createStatement();
            ResultSet result1 = cmd1.executeQuery(aff);
            
            epic.table.setRowCount(0);
            epic.table.setColumnIdentifiers(titre);
            Object[] row = new Object[4];
            while(result1.next())
            {
                row[0]= result1.getInt("id");
                row[1]= result1.getString("nom");
                row[2]= result1.getString("prix");
                row[3]= result1.getString("marque");                
                epic.table.setColumnCount(4);
                epic.table.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(bdShow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void showViande() {
                     try {
            String titre[] ={"ID","NOM","PRIX","ANIMAL"};
            String aff = "select * from viande;";
            String url = "jdbc:mysql://localhost:3306/epicerie?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            String user = "root";
            String pw = "";
            Connection conn= DriverManager.getConnection(url, user, pw);
            Statement cmd1 = conn.createStatement();
            ResultSet result1 = cmd1.executeQuery(aff);
            
            epic.table.setRowCount(0);
            epic.table.setColumnIdentifiers(titre);
            Object[] row = new Object[4];
            while(result1.next())
            {
                row[0]= result1.getInt("id");
                row[1]= result1.getString("nom");
                row[2]= result1.getString("prix");
                row[3]= result1.getString("animal");                
                epic.table.setColumnCount(4);
                epic.table.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(bdShow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
}
