/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame
{
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    
    
    private InetAddress adrs;
    
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private JTextField txtIpAdress;
    private JRadioButton rdserv;
    private JRadioButton rdClie;
    private BackGroundCom com;
    private Thread t;
    private Thread t1;
    private GridLayout grid;
    
   
    
    public BattleSea()
    {
        this.setSize(500,425);
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector();
               
        this.add(btnStartServer);
        this.add(txtPortNumber);
        this.add(txtIpAdress);
    
        
         CreateShips();
       
        
     
       
        
    }
    
    
    private void createGrid()
    {grid=new GridLayout(11,10);
    this.setLayout(grid);
    this.setSize(1000,1000);
        listbtn = new JButton[100];
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10%(battleship.BattleSea.WIDTH),10%(BattleSea.HEIGHT));
         listbtn[i].setBackground(java.awt.Color.BLUE);          
         this.add(listbtn[i]);
        
        }
//          this.add(txtIpAdress) ;
//        this.add(rdserv);
//        this.add(rdClie);
//          
      // rdserv=new JRadioButton("Serveur");
      
//      JTextField ipad=new JTextField();
//      txtIpAdress.setText("Entrez L'adresse ip d votre Serveur");
//       JRadioButton rdClie= new JRadioButton("Client");
//       JRadioButton rdServ= new JRadioButton("Serveur");
//       ButtonGroup groupe=new ButtonGroup();
//       groupe.add(rdServ);
//       groupe.add(rdClie);
//       add(rdServ);
//       add(rdClie);
//        if (rdServ.isSelected()) 
//{
//            
        
        
        btnStartServer = new JButton("Start Server");
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
               com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
              t = new Thread(com);
               t.start();
            }
        
        });
        txtPortNumber = new JTextField();
        
        txtPortNumber.setText("Enter Port number");
         
// }
//else if (rdClie.isSelected()) 
//{
//        
//        btnStartServer = new JButton("Start As Client");
//        btnStartServer.addActionListener(new ActionListener()
//        {
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//               com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()));
//              t1 = new Thread(com);
//               t1.start();
//            }
//        
//        });
//        txtPortNumber = new JTextField();
//        txtPortNumber.setText("Enter Port number");
//        txtIpAdress=new JTextField();
//        txtIpAdress.setText("Enter adresse du serveur");
//        
//        
//}       

}

    
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
                
                com.missileOutgoing = Integer.parseInt(ae.getActionCommand());
                com.dataToSend = true;
                 
                  
             }    
          });
        }
         
    }
    
   
       
    private void CreateShips()
    {
        int head ,nb=0 ,dispo;
        Random r = new Random();
        listShip = new ArrayList<Ship>();
        listJButtonInShip=new ArrayList<JButton>();
        
      dispo=r.nextInt(2)+1;
      if (dispo==1)
      {dispo=1;}
      else
      {
          dispo=10;
      }
            
        
       // for(int i = 0;i<5;i++)
      //  {  
               for(int i = 0;i<5;i++)
       {
          
            try
            {//int i=1;
               ArrayList<JButton> boatsection = new ArrayList<JButton>();
              
                 head = r.nextInt(98)+1;
                 nb = r.nextInt(3)+1;
                  for (int j = 7; j < 100; j+=10) {
                 
                  if (head==j||head==j+1||head==j+2) {
                      head-=3;
                    
                }else{ head=head;
                  }
               
                 for(int cnt=0;cnt< nb ;cnt++)
                 {
                   boatsection.add(listbtn[head+(cnt*dispo)]);
                   listJButtonInShip.add(listbtn[head+(i*dispo)]);
                 }
                 
                 Ship s = new Ship(boatsection);
                 listShip.add(s);
                  }
                  JOptionPane.showMessageDialog(null,listShip.size() );
            }
            catch(Exception ex)
            {
                
            }
    }
         
            
        
        
        
    
    }
    
    public static void UpdateGrig(int incomming)
    {
        for(int i = 0; i<listbtn.length;i++)
        {
           
                
                  for(Ship element:listShip)
                  {
                      element.checkHit(listbtn[incomming]);
                     
                  }  
                 
                  
                
          
        }

         
    }
}
